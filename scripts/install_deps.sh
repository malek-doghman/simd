mkdir third-parties && cd third-parties

# Installing the latest cmake from source
apt-get update
apt-get --assume-yes install build-essential
wget http://www.cmake.org/files/v3.11/cmake-3.11.0.tar.gz
tar xf cmake-3.11.0.tar.gz
cd cmake-3.11.0
./configure
make -j8
apt-get install checkinstall
checkinstall
cd ..

# Installing Google Test
git clone https://github.com/google/googletest.git
cd googletest
mkdir build && cd build
cmake ..
make -j8
make install
cd ../..

# Installing Google Benchmark
git clone https://github.com/google/benchmark.git
cd benchmark
mkdir build && cd build
cmake .. -DCMAKE_BUILD_TYPE=RELEASE
make -j8
make install
cd ../..

#Leaving third-parties
cd ..
