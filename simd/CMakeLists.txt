add_library(libsimd "")
set_target_properties(libsimd PROPERTIES LINKER_LANGUAGE CXX)
target_sources(libsimd
    PUBLIC
        "${CMAKE_CURRENT_LIST_DIR}/simd_check.h"
        "${CMAKE_CURRENT_LIST_DIR}/uintn.h"
        "${CMAKE_CURRENT_LIST_DIR}/simd4i.h"
        "${CMAKE_CURRENT_LIST_DIR}/simd8f.h"
        "${CMAKE_CURRENT_LIST_DIR}/simd8i.h"
        "${CMAKE_CURRENT_LIST_DIR}/simd4f.h"
)
target_include_directories (libsimd PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_compile_options(libsimd PUBLIC -std=c++11 -w -Wall )