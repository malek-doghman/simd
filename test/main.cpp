#include <iostream>
#include <gtest/gtest.h>
#include "simd4i.h"
#include "simd8i.h"

TEST(Vec4iSumTest, HandlesZeroInput) {    
   dlib::simd4i vec1(0, 0, 0, 0);
   dlib::simd4i vec2(0, 0, 0, 0);
   dlib::simd4i vec3 = vec1 + vec2;
   EXPECT_EQ(0, vec3[0]);
   EXPECT_EQ(0, vec3[1]);
   EXPECT_EQ(0, vec3[2]);
   EXPECT_EQ(0, vec3[3]);
}

TEST(Vec8iSumTest, HandlesZeroInput) {    
   dlib::simd8i vec1(0, 0, 0, 0, 0, 0, 0, 0);
   dlib::simd8i vec2(0, 0, 0, 0, 0, 0, 0, 0);
   dlib::simd8i vec3 = vec1 + vec2;
   EXPECT_EQ(0, vec3[0]);
   EXPECT_EQ(0, vec3[1]);
   EXPECT_EQ(0, vec3[2]);
   EXPECT_EQ(0, vec3[3]);
   EXPECT_EQ(0, vec3[4]);
   EXPECT_EQ(0, vec3[5]);
   EXPECT_EQ(0, vec3[6]);
   EXPECT_EQ(0, vec3[7]);
}

GTEST_API_ int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
