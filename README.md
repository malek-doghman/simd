# SIMD
Code took from DLIB
https://github.com/davisking/dlib

## Dependencies
- git
- cmake
- gcc/g++

## Building [Linux Ubuntu 16.04]
```bash
git clone https://malek-doghman@bitbucket.org/malek-doghman/simd.git
cd simd
./scripts/rebuild.sh
```