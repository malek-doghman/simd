#include <benchmark/benchmark.h>
#include "simd4i.h"
#include "simd8i.h"
static void BM_Vec4iSum(benchmark::State& state) {
    dlib::simd4i vec1(0, 0, 0, 0);
    dlib::simd4i vec2(0, 0, 0, 0);
    dlib::simd4i vec3 = vec1 + vec2;
}
// Register the function as a benchmark
BENCHMARK(BM_Vec4iSum);

// Define another benchmark
static void BM_SimpleVecSum(benchmark::State& state) {
  std::vector<int> vec1{0, 0, 0, 0};
  std::vector<int> vec2{0, 0, 0, 0};
  std::vector<int> vec3{0, 0, 0, 0};
  for (size_t i = 0; i < 4; i++)
  {
      vec3[i] = vec1[i] + vec2[i];
  }
}
BENCHMARK(BM_SimpleVecSum);

BENCHMARK_MAIN();